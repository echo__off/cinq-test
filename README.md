# Cinq test

### Installation
>npm install

### Running
> ng serve

### Unit tests
> ng test

### Automated tests
> ng e2e

### Notes
> Clicking in the navbar link will return to the main route

