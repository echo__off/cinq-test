import { AppPage } from './app.po';

describe('cinq-test App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    page.maximizeWindow();
    expect(page.getTitleText()).toEqual('Cinq Test');
    page.getTableUserByIndex(1).click();
    expect(page.getUserDetailsTitleText()).toEqual('User details');
    page.getEditButton().click();
    page.setFirstNameInput('Jack');
    page.setLastNameInput('Frost');
    page.getSaveButton().click();
    page.getBackButton().click();
    expect(page.getTableUserNameByIndex(1).getText()).toEqual('Jack Frost');
  });
});
