import { browser, by, element, protractor } from 'protractor';

export class AppPage {
  private ec = protractor.ExpectedConditions;
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }

  maximizeWindow() {
    browser.driver.manage().window().maximize();
  }

  getTitleText() {
    return element(by.css('.navbar-brand')).getText();
  }

  getUserDetailsTitleText() {
    return element(by.css('app-user-details h3')).getText();
  }

  getTableUserByIndex(id: number) {
    let el = element(by.css('app-user-table tr:nth-child(' + id + ') a.show'));
    browser.wait(this.ec.visibilityOf(el), 4000);
    return el;
  }

  getEditButton() {
    let button = element(by.css('#edit'))
    browser.wait(this.ec.visibilityOf(button), 4000);
    return button;
  }

  setFirstNameInput(name: string) {
    let input = element(by.css('input[name="firstName"]'))
    browser.wait(this.ec.visibilityOf(input), 4000);
    input.clear();
    input.sendKeys(name);
  }

  setLastNameInput(name: string) {
    let input = element(by.css('input[name="lastName"]'))
    browser.wait(this.ec.visibilityOf(input), 4000);
    input.clear();
    input.sendKeys(name);
  }

  getSaveButton() {
    let button = element(by.css('#save'))
    browser.wait(this.ec.visibilityOf(button), 4000);
    return button;
  }

  getBackButton() {
    let button = element(by.css('#back'));
    browser.wait(this.ec.visibilityOf(button), 4000);
    return button;
  }

  getTableUserNameByIndex(id: number) {
    let el = element(by.css('app-user-table tr:nth-child(' + id + ') .user-name'));
    browser.wait(this.ec.visibilityOf(el), 4000);
    return el;
  }
}
