import { TestBed, async }          from '@angular/core/testing';
import { RouterTestingModule }     from '@angular/router/testing'
import { AppComponent }            from './app.component';
import { routes }                  from './app.module'
import { FilterPipe }              from './pipes/filter.pipe';
import { FormsModule }             from '@angular/forms';
import { AppUserComponent }        from './components/app-user/app-user.component'
import { AppUserCardsComponent }   from './components/app-user-cards/app-user-cards.component';
import { AppUserTableComponent }   from './components/app-user-table/app-user-table.component';
import { AppUserDetailsComponent } from './components/app-user-details/app-user-details.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        AppUserComponent,
        AppUserCardsComponent,
        AppUserTableComponent,
        AppUserDetailsComponent,
        FilterPipe
      ],
      imports: [RouterTestingModule.withRoutes(routes), FormsModule]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'Cinq Test'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Cinq Test');
  }));
});
