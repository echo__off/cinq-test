import { HttpClientModule }              from '@angular/common/http';
import { NgModule }                      from '@angular/core';
import { FormsModule }                   from '@angular/forms';
import { BrowserModule }                 from '@angular/platform-browser';
import { RouterModule, Routes, Router }  from '@angular/router';
import { NgbModule }                     from '@ng-bootstrap/ng-bootstrap';
import { AppComponent }                  from './app.component';
import { AppUserCardsComponent }         from './components/app-user-cards/app-user-cards.component';
import { AppModalConfirmationComponent } from './components/app-modal-confirmation/app-modal-confirmation.component';
import { AppUserComponent }              from './components/app-user/app-user.component';
import { AppUserDetailsComponent }       from './components/app-user-details/app-user-details.component';
import { AppUserTableComponent }         from './components/app-user-table/app-user-table.component';
import { AppLoadModule }                 from './modules/app-load/app-load.module';
import { FilterPipe }                    from './pipes/filter.pipe';

export const routes: Routes = [
  { path: '', component: AppUserComponent },
  { path: 'user/:id', component: AppUserDetailsComponent, }
];

@NgModule({
  declarations: [
    AppComponent,
    AppModalConfirmationComponent,
    AppUserCardsComponent,
    AppUserComponent,
    AppUserTableComponent,
    FilterPipe,
    AppUserDetailsComponent
  ],
  imports: [
    AppLoadModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    RouterModule.forRoot(routes, {})
  ],
  bootstrap: [AppComponent],
  entryComponents: [AppModalConfirmationComponent]
})

export class AppModule { }
