import { async, ComponentFixture, TestBed }   from '@angular/core/testing';
import { AppUserComponent }                   from './app-user.component';
import { RouterTestingModule }                from '@angular/router/testing'
import { routes }                             from '../../app.module'
import { FilterPipe }                         from '../../pipes/filter.pipe';
import { FormsModule }                        from '@angular/forms';
import { NgbModule, NgbActiveModal }          from '@ng-bootstrap/ng-bootstrap';
import { Router }                             from '@angular/router';
import { AppUserDetailsComponent }            from '../../components/app-user-details/app-user-details.component';
import { AppUserCardsComponent }              from '../../components/app-user-cards/app-user-cards.component';
import { AppUserTableComponent }              from '../../components/app-user-table/app-user-table.component';
import { UserDataService}                     from '../../services/user-data.service';
import { HttpClient, HttpHandler }            from '@angular/common/http';
import { inject }                             from '@angular/core/testing';

describe('AppUserComponent', () => {
  let component: AppUserComponent;
  let fixture: ComponentFixture<AppUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppUserComponent,
        FilterPipe,
        AppUserDetailsComponent,
        AppUserCardsComponent,
        AppUserTableComponent
      ],
      providers: [
        UserDataService, 
        HttpClient, 
        HttpHandler, 
        NgbActiveModal,
        FilterPipe
      ],
      imports: [
        FormsModule,
        RouterTestingModule.withRoutes(routes),
        NgbModule.forRoot()
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('expect userData length to be 18', () => {
    expect(component.userData.length === 18).toBeTruthy();
  });

  it('select all items and expect to be selected 18', () => {
    component.userData.forEach(el => el.checked = true);
    expect(component.selected()).toEqual('18 selected');
  });

  it('filter for Ash and expect it to result be 1 user', inject(
    [FilterPipe],
    (service: FilterPipe) => 
  {
    let users = service.transform(component.userData, 'Ash', 'firstName');
    expect(users.length).toEqual(1);
  }));

  it('delete all itens and expect to be 0 left', () => {
    component.userData.forEach(el => el.checked = true);
    component.deleteSelected()
    expect(component.userData.length).toEqual(0);
  });
});
