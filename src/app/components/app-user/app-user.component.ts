import { Component, OnInit }             from '@angular/core';
import { NgIf }                          from '@angular/common';
import { NgbModal }                      from '@ng-bootstrap/ng-bootstrap';
import { User }                          from  '../../interfaces/user';
import { FilterPipe }                    from '../../pipes/filter.pipe';
import { AppModalConfirmationComponent } from '../app-modal-confirmation/app-modal-confirmation.component';
import { UserDataService }               from '../../services/user-data.service';


@Component({
  selector: 'app-user',
  templateUrl: './app-user.component.html',
  styleUrls: ['./app-user.component.css']
})
export class AppUserComponent implements OnInit {
  userData: User[];
  modalRef: any;
  searchText: string;
  isMobile: boolean;

  constructor(private modalService: NgbModal, 
              private userService: UserDataService
  ) { }

  ngOnInit() {
    this.isMobile = Math.max(document.documentElement.clientWidth, window.innerWidth || 0) < 1024;

    this.userData = JSON.parse(localStorage.getItem('users'));;
    console.log('Loaded user data from localStorage');
  }

  public selected(): string {
    return this.userData.filter(el => el.checked).length + ' selected';
  }

  onResize(event): void {
    this.isMobile = event.target.innerWidth < 1024;
  }

  deleteUser(user: User): void {
    this.modalRef = this.modalService.open(AppModalConfirmationComponent);
    
    this.modalRef.result.then((confirmed) => {
      if (confirmed) {
        let index = this.getIndex(user.id);
        this.userData.splice(index, 1);

        this.updateLocalStorage();
      }
    }, (reason) => {
      console.log('Dismissed');
    });
  }

  isButtonDisabled(): boolean {
    return this.userData.filter(el => el.checked).length === 0;
  }

  private updateLocalStorage(): void {
    this.userService.saveUsers(this.userData);
  }

  private getIndex(id: number): number {
    let idx;

    this.userData.forEach((user, index) => {
      if(user.id === id) idx = index;
    });

    return idx;
  }

  downloadData(): void {
    let dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(this.userData.filter(el => el.checked), null, 4));
    let downloadElement = document.createElement('a');

    downloadElement.setAttribute('href',     dataStr);
    downloadElement.setAttribute('download', 'users.json');
    downloadElement.click();
    downloadElement.remove();
  }

  deleteSelected(): void {
    this.userData.filter(el => el.checked)
                 .forEach((user) => {
                    let index = this.getIndex(user.id);
                    this.userData.splice(index, 1);
                 });

    this.updateLocalStorage();
  }
}
