import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule }                      from '@angular/forms';
import { AppUserCardsComponent }            from './app-user-cards.component';
import { RouterTestingModule }              from '@angular/router/testing'
import { routes }                           from '../../app.module'
import { AppUserComponent }                 from '../../components/app-user/app-user.component'
import { AppUserDetailsComponent }          from '../../components/app-user-details/app-user-details.component';
import { AppUserTableComponent}             from '../../components/app-user-table/app-user-table.component';
import { FilterPipe }                       from '../../pipes/filter.pipe';

describe('AppCardsComponent', () => {
  let component: AppUserCardsComponent;
  let fixture: ComponentFixture<AppUserCardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppUserCardsComponent,
        AppUserComponent,
        AppUserDetailsComponent,
        FilterPipe,
        AppUserTableComponent
      ],
      imports: [FormsModule, RouterTestingModule.withRoutes(routes)]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppUserCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
