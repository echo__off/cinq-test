import { Component, Input, Output, EventEmitter }  from '@angular/core';
import { User }                                    from  '../../interfaces/user';

@Component({
  selector: 'app-user-cards',
  templateUrl: './app-user-cards.component.html',
  styleUrls: ['./app-user-cards.component.css']
})
export class AppUserCardsComponent {
  @Input('user-data') userData: User[];
  @Output() deletion = new EventEmitter<object>();

  public deleteUser(user): void {
    this.deletion.emit(user);
  }
}
