import { Component, OnDestroy, Input } from '@angular/core';
import { NgbActiveModal }              from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-confirmation',
  templateUrl: './app-modal-confirmation.component.html',
  styleUrls: ['./app-modal-confirmation.component.css']
})
export class AppModalConfirmationComponent implements OnDestroy {
  @Input() user: string;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnDestroy() {}
}