import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NgbActiveModal }                   from '@ng-bootstrap/ng-bootstrap';
import { FormsModule }                      from '@angular/forms';
import { AppModalConfirmationComponent }    from './app-modal-confirmation.component';

describe('AppModalConfirmationComponent', () => {
  let component: AppModalConfirmationComponent;
  let fixture: ComponentFixture<AppModalConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppModalConfirmationComponent ],
      providers: [NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppModalConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
