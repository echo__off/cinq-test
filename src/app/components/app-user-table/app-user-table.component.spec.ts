import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppUserTableComponent }            from './app-user-table.component';
import { RouterTestingModule }              from '@angular/router/testing'
import { routes }                           from '../../app.module'
import { FilterPipe }                       from '../../pipes/filter.pipe';
import { FormsModule }                      from '@angular/forms';
import { AppUserComponent}                  from '../../components/app-user/app-user.component';
import { AppUserDetailsComponent }          from '../../components/app-user-details/app-user-details.component';
import { AppUserCardsComponent }            from '../../components/app-user-cards/app-user-cards.component';

describe('AppUserTableComponent', () => {
  let component: AppUserTableComponent;
  let fixture: ComponentFixture<AppUserTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppUserTableComponent, 
        FilterPipe, 
        AppUserComponent, 
        AppUserDetailsComponent, 
        AppUserCardsComponent 
      ],
      imports: [FormsModule, RouterTestingModule.withRoutes(routes)]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppUserTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
