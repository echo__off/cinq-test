import { Component, Input, Output, EventEmitter }  from '@angular/core';
import { User }                                    from  '../../interfaces/user';

@Component({
  selector: 'app-user-table',
  templateUrl: './app-user-table.component.html',
  styleUrls: ['./app-user-table.component.css']
})
export class AppUserTableComponent {
  @Input('user-data') userData: User[];
  @Output() deletion = new EventEmitter<object>();
  
  allSelected: boolean;

  public deleteUser(user): void {
    this.deletion.emit(user);
  }

  selectAll(): void {
    this.userData.forEach((el) => {
      el.checked = !this.allSelected;
    })
  }
}
