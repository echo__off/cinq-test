import { Component, OnInit }             from '@angular/core';
import { ActivatedRoute, Router }        from '@angular/router';
import { User }                          from '../../interfaces/user';
import { NgbModal }                      from '@ng-bootstrap/ng-bootstrap';
import { AppModalConfirmationComponent } from '../app-modal-confirmation/app-modal-confirmation.component';
import { UserDataService }               from '../../services/user-data.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './app-user-details.component.html',
  styleUrls: ['./app-user-details.component.css']
})
export class AppUserDetailsComponent implements OnInit {
  id: number;
  userData: User[];
  userBkp: User;
  user: User;
  modalRef: any;

  constructor(private route: ActivatedRoute,
              private modalService: NgbModal, 
              private userService: UserDataService,
              private router: Router
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = parseInt(params.get('id'));
    });

    this.userData = JSON.parse(localStorage.getItem('users'));
    this.backupUsers(this.userData);
    this.resetUserData();
  }

  back(): void {
    if (JSON.stringify(this.user) === JSON.stringify(this.userBkp)) {
      this.router.navigate(['/']);
      return;
    };

    this.modalRef = this.modalService.open(AppModalConfirmationComponent);
    
    this.modalRef.result.then((confirmed) => {
      if (confirmed) {
        this.resetUserData();
        this.router.navigate(['/']);
      }
    }, (reason) => {
      console.log('Dismissed');
    });
  }

  saveChanges(): void {
    this.userData.some((el, i) => {
      if (el.id === this.user.id) {
          this.userData[i] = this.deepCopy(this.user);
          return true;
      }

      return false;
    });

    this.userService.saveUsers(this.userData);
    this.backupUsers(this.userData);
  }

  private backupUsers(users: User[]): void {
    this.userBkp = users.filter(el => el.id === this.id)[0];
  }

  private resetUserData(): void {
    this.user = this.deepCopy(this.userBkp);
  }

  private deepCopy(data: User): User {
    return JSON.parse(JSON.stringify(data));
  }

}
