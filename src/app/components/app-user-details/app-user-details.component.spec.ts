import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppUserDetailsComponent }          from './app-user-details.component';
import { FormsModule }                      from '@angular/forms';
import { RouterTestingModule }              from '@angular/router/testing';
import { routes }                           from '../../app.module';
import { AppUserComponent }                 from '../../components/app-user/app-user.component'
import { FilterPipe }                       from '../../pipes/filter.pipe';
import { NgbModal, NgbModule }              from '@ng-bootstrap/ng-bootstrap';
import { AppUserCardsComponent }            from '../../components/app-user-cards/app-user-cards.component';
import { AppUserTableComponent }            from '../../components/app-user-table/app-user-table.component';
import { UserDataService }                  from '../../services/user-data.service';
import { HttpClient, HttpHandler }          from '@angular/common/http';


describe('AppUserDetailsComponent', () => {
  let component: AppUserDetailsComponent;
  let fixture: ComponentFixture<AppUserDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppUserDetailsComponent,
        AppUserComponent, 
        FilterPipe, 
        AppUserCardsComponent, 
        AppUserTableComponent
      ],
      providers: [
        NgbModal, 
        UserDataService, 
        HttpClient, 
        HttpHandler
      ],
      imports: [
        FormsModule, 
        RouterTestingModule.withRoutes(routes), 
        NgbModule.forRoot()
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppUserDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
