import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, NgModule }    from '@angular/core';
import { Observable }              from 'rxjs/Observable';
import { User }                    from '../interfaces/user';

@Injectable()
export class UserDataService {
  users: User[]

  constructor(private http: HttpClient) { }

  public getUsers(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getJson().subscribe(
        data => resolve(JSON.stringify(data)),
        err => reject(err),
        () => console.log('Done loading user data from mock JSON')
      );
    })
  }

  public saveUsers(userData: User[]): void {
    // deep copy so we remove the checked attribute without affecting the view
    let userDataCopy = JSON.parse(JSON.stringify(userData));
    userDataCopy.forEach(el => delete el.checked);
    window.localStorage.setItem('users', JSON.stringify(userDataCopy));
    console.log('localStorage data updated');
  }

  public getJson(): any {
    return this.http.get<User[]>('/assets/users.json');
  }
}
