import { TestBed, inject }         from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { UserDataService }         from './user-data.service';

describe('UserDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        UserDataService,
        HttpClient,
        HttpHandler
      ]
    });
  });
});
