import { CommonModule }              from '@angular/common';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { UserDataService }           from '../../services/user-data.service';

export function init(userDataService: UserDataService) {
    return () => userDataService.getUsers().then((userData) => {
      console.log('Saved userData to localStorage');
      window.localStorage.setItem('users', userData);
    });
}

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
      UserDataService,
      {
        provide: APP_INITIALIZER,
        useFactory: init,
        deps: [UserDataService],
        multi: true
      }
  ],
  declarations: []
})
export class AppLoadModule { }
